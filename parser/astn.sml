(* The abstract syntax tree for expression *)

structure A =
struct

type pos = int

datatype Types =   INT
					|STR

datatype BinOp = Plus | Minus | Mul | Div;

datatype RelOp = GtOp|GteOp|LtOp|LteOp|EqOp|NeqOp

datatype LoOp = AndOp|OrOp

datatype Variable = Var of string
  
datatype Intconstant = Int of int

datatype StrConst = Str of string
					 
datatype MathExp = 	IntExp of int
						|VarExp of Variable
						|BinE of MathExp * BinOp * MathExp

datatype Condition =    ExpC of MathExp
						|RelC of MathExp * RelOp * MathExp
						|LoC of Condition * LoOp * Condition
						|NotC of Condition



(* The abstract syntax for expressions *)

datatype Statement = Dec of  Types * Variable list
					  |Assign of Variable * MathExp  
					  |If of Condition * Statement list
					  |IfElse of Condition * Statement list * Statement list 
					  |While of Condition * Statement list
					  |Break
					  |Continue
					  
                      					  
type Programme = Statement list

end;


 


(* Some helper functions *)
(*fun plus  a b = Op (a, Plus, b)
fun minus a b = Op (a, Minus, b)
fun mul   a b = Op (a, Mul, b)
*)
