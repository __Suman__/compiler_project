functor CLLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : CL_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct

end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\015\000\002\000\014\000\003\000\013\000\024\000\012\000\
\\028\000\011\000\030\000\010\000\031\000\009\000\000\000\
\\001\000\001\000\017\000\000\000\
\\001\000\001\000\031\000\004\000\030\000\008\000\029\000\022\000\028\000\000\000\
\\001\000\001\000\031\000\004\000\030\000\008\000\034\000\000\000\
\\001\000\001\000\036\000\000\000\
\\001\000\007\000\019\000\000\000\
\\001\000\007\000\020\000\000\000\
\\001\000\007\000\024\000\000\000\
\\001\000\007\000\056\000\010\000\047\000\011\000\046\000\012\000\045\000\
\\013\000\044\000\000\000\
\\001\000\007\000\075\000\000\000\
\\001\000\007\000\076\000\000\000\
\\001\000\007\000\079\000\000\000\
\\001\000\008\000\021\000\000\000\
\\001\000\008\000\022\000\000\000\
\\001\000\009\000\051\000\020\000\050\000\021\000\049\000\000\000\
\\001\000\009\000\055\000\020\000\050\000\021\000\049\000\000\000\
\\001\000\009\000\066\000\010\000\047\000\011\000\046\000\012\000\045\000\
\\013\000\044\000\000\000\
\\001\000\009\000\067\000\020\000\050\000\021\000\049\000\000\000\
\\001\000\023\000\023\000\000\000\
\\001\000\025\000\074\000\027\000\073\000\000\000\
\\001\000\026\000\068\000\000\000\
\\001\000\027\000\078\000\000\000\
\\001\000\029\000\065\000\000\000\
\\001\000\032\000\072\000\000\000\
\\001\000\033\000\000\000\000\000\
\\081\000\000\000\
\\082\000\000\000\
\\083\000\001\000\015\000\002\000\014\000\003\000\013\000\024\000\012\000\
\\028\000\011\000\030\000\010\000\031\000\009\000\000\000\
\\084\000\000\000\
\\085\000\000\000\
\\086\000\000\000\
\\087\000\000\000\
\\088\000\000\000\
\\089\000\000\000\
\\090\000\000\000\
\\091\000\000\000\
\\092\000\005\000\025\000\000\000\
\\093\000\000\000\
\\094\000\005\000\058\000\000\000\
\\095\000\000\000\
\\096\000\000\000\
\\097\000\000\000\
\\098\000\000\000\
\\099\000\000\000\
\\100\000\012\000\045\000\013\000\044\000\000\000\
\\101\000\012\000\045\000\013\000\044\000\000\000\
\\102\000\000\000\
\\103\000\000\000\
\\104\000\000\000\
\\105\000\000\000\
\\106\000\000\000\
\\107\000\000\000\
\\108\000\000\000\
\\109\000\000\000\
\\110\000\000\000\
\\111\000\000\000\
\\112\000\000\000\
\\113\000\000\000\
\\114\000\000\000\
\\115\000\010\000\047\000\011\000\046\000\012\000\045\000\013\000\044\000\000\000\
\\116\000\020\000\050\000\021\000\049\000\000\000\
\\117\000\009\000\066\000\010\000\047\000\011\000\046\000\012\000\045\000\
\\013\000\044\000\014\000\043\000\015\000\042\000\016\000\041\000\
\\017\000\040\000\018\000\039\000\019\000\038\000\000\000\
\\117\000\010\000\047\000\011\000\046\000\012\000\045\000\013\000\044\000\
\\014\000\043\000\015\000\042\000\016\000\041\000\017\000\040\000\
\\018\000\039\000\019\000\038\000\000\000\
\\118\000\000\000\
\\119\000\000\000\
\"
val actionRowNumbers =
"\000\000\028\000\001\000\030\000\
\\029\000\027\000\025\000\005\000\
\\006\000\012\000\013\000\034\000\
\\033\000\018\000\007\000\036\000\
\\026\000\032\000\031\000\002\000\
\\002\000\003\000\035\000\004\000\
\\062\000\014\000\002\000\002\000\
\\041\000\042\000\015\000\008\000\
\\003\000\037\000\038\000\003\000\
\\056\000\055\000\054\000\053\000\
\\052\000\051\000\003\000\003\000\
\\003\000\003\000\002\000\058\000\
\\057\000\022\000\063\000\061\000\
\\017\000\020\000\040\000\016\000\
\\001\000\059\000\047\000\046\000\
\\045\000\044\000\060\000\000\000\
\\043\000\064\000\000\000\039\000\
\\023\000\019\000\009\000\010\000\
\\000\000\050\000\048\000\021\000\
\\011\000\049\000\024\000"
val gotoT =
"\
\\001\000\078\000\002\000\006\000\003\000\005\000\004\000\004\000\
\\006\000\003\000\009\000\002\000\010\000\001\000\000\000\
\\000\000\
\\011\000\014\000\000\000\
\\000\000\
\\000\000\
\\002\000\016\000\003\000\005\000\004\000\004\000\006\000\003\000\
\\009\000\002\000\010\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\025\000\007\000\024\000\000\000\
\\005\000\030\000\007\000\024\000\000\000\
\\007\000\031\000\000\000\
\\000\000\
\\012\000\033\000\000\000\
\\008\000\035\000\000\000\
\\013\000\046\000\000\000\
\\005\000\050\000\007\000\024\000\000\000\
\\005\000\052\000\007\000\051\000\000\000\
\\000\000\
\\000\000\
\\013\000\046\000\000\000\
\\000\000\
\\007\000\055\000\000\000\
\\000\000\
\\000\000\
\\007\000\057\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\058\000\000\000\
\\007\000\059\000\000\000\
\\007\000\060\000\000\000\
\\007\000\061\000\000\000\
\\005\000\062\000\007\000\024\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\013\000\046\000\000\000\
\\008\000\035\000\000\000\
\\013\000\046\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\011\000\067\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\013\000\046\000\000\000\
\\002\000\068\000\003\000\005\000\004\000\004\000\006\000\003\000\
\\009\000\002\000\010\000\001\000\000\000\
\\000\000\
\\000\000\
\\002\000\069\000\003\000\005\000\004\000\004\000\006\000\003\000\
\\009\000\002\000\010\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\075\000\003\000\005\000\004\000\004\000\006\000\003\000\
\\009\000\002\000\010\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 79
val numrules = 39
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | IntConst of unit ->  (int) | ID of unit ->  (string)
 | LoOp of unit ->  (A.LoOp) | Decr of unit ->  (A.Variable list)
 | Dec of unit ->  (A.Variable list) | Decs of unit ->  (A.Statement)
 | Type of unit ->  (A.Types) | RelOp of unit ->  (A.RelOp)
 | MathExp of unit ->  (A.MathExp) | Control of unit ->  (A.Statement)
 | Condition of unit ->  (A.Condition)
 | Assignment of unit ->  (A.Statement)
 | Statement of unit ->  (A.Statement)
 | Statements of unit ->  (A.Statement list)
 | Program of unit ->  (A.Statement list)
end
type svalue = MlyValue.svalue
type result = A.Statement list
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn (T 27) => true | (T 28) => true | (T 31) => true | (T 29) => true
 | (T 30) => true | (T 23) => true | (T 25) => true | (T 24) => true
 | (T 26) => true | (T 1) => true | (T 2) => true | _ => false
val preferred_change : (term list * term list) list = 
nil
val noShift = 
fn (T 32) => true | _ => false
val showTerminal =
fn (T 0) => "ID"
  | (T 1) => "INT"
  | (T 2) => "STRING"
  | (T 3) => "IntConst"
  | (T 4) => "COMMA"
  | (T 5) => "COLON"
  | (T 6) => "SEMICOLON"
  | (T 7) => "LPAREN"
  | (T 8) => "RPAREN"
  | (T 9) => "PLUS"
  | (T 10) => "MINUS"
  | (T 11) => "MUL"
  | (T 12) => "DIV"
  | (T 13) => "EQ"
  | (T 14) => "NEQ"
  | (T 15) => "LT"
  | (T 16) => "LE"
  | (T 17) => "GT"
  | (T 18) => "GE"
  | (T 19) => "AND"
  | (T 20) => "OR"
  | (T 21) => "NOT"
  | (T 22) => "ASSIGN"
  | (T 23) => "IF"
  | (T 24) => "ELSE"
  | (T 25) => "THEN"
  | (T 26) => "ENDIF"
  | (T 27) => "WHILE"
  | (T 28) => "DO"
  | (T 29) => "BREAK"
  | (T 30) => "CONTINUE"
  | (T 31) => "ENDWHILE"
  | (T 32) => "EOF"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 32) $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27) $$ (T 26)
 $$ (T 25) $$ (T 24) $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20) $$ (T 19)
 $$ (T 18) $$ (T 17) $$ (T 16) $$ (T 15) $$ (T 14) $$ (T 13) $$ (T 12)
 $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ 
(T 4) $$ (T 2) $$ (T 1)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.Statements Statements1, Statements1left, 
Statements1right)) :: rest671)) => let val  result = MlyValue.Program
 (fn _ => let val  (Statements as Statements1) = Statements1 ()
 in ( Statements )
end)
 in ( LrTable.NT 0, ( result, Statements1left, Statements1right), 
rest671)
end
|  ( 1, ( ( _, ( MlyValue.Statements Statements1, _, Statements1right)
) :: ( _, ( MlyValue.Statement Statement1, Statement1left, _)) :: 
rest671)) => let val  result = MlyValue.Statements (fn _ => let val  (
Statement as Statement1) = Statement1 ()
 val  (Statements as Statements1) = Statements1 ()
 in ( Statement :: Statements )
end)
 in ( LrTable.NT 1, ( result, Statement1left, Statements1right), 
rest671)
end
|  ( 2, ( ( _, ( MlyValue.Statement Statement1, Statement1left, 
Statement1right)) :: rest671)) => let val  result = 
MlyValue.Statements (fn _ => let val  (Statement as Statement1) = 
Statement1 ()
 in ( [Statement] )
end)
 in ( LrTable.NT 1, ( result, Statement1left, Statement1right), 
rest671)
end
|  ( 3, ( ( _, ( MlyValue.Decs Decs1, Decs1left, Decs1right)) :: 
rest671)) => let val  result = MlyValue.Statement (fn _ => let val  (
Decs as Decs1) = Decs1 ()
 in (Decs)
end)
 in ( LrTable.NT 2, ( result, Decs1left, Decs1right), rest671)
end
|  ( 4, ( ( _, ( MlyValue.Assignment Assignment1, Assignment1left, 
Assignment1right)) :: rest671)) => let val  result = 
MlyValue.Statement (fn _ => let val  (Assignment as Assignment1) = 
Assignment1 ()
 in (Assignment)
end)
 in ( LrTable.NT 2, ( result, Assignment1left, Assignment1right), 
rest671)
end
|  ( 5, ( ( _, ( MlyValue.Control Control1, Control1left, 
Control1right)) :: rest671)) => let val  result = MlyValue.Statement
 (fn _ => let val  (Control as Control1) = Control1 ()
 in (Control)
end)
 in ( LrTable.NT 2, ( result, Control1left, Control1right), rest671)

end
|  ( 6, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( _, BREAK1left, _))
 :: rest671)) => let val  result = MlyValue.Statement (fn _ => (
A.Break))
 in ( LrTable.NT 2, ( result, BREAK1left, SEMICOLON1right), rest671)

end
|  ( 7, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( _, CONTINUE1left, _
)) :: rest671)) => let val  result = MlyValue.Statement (fn _ => (
A.Continue))
 in ( LrTable.NT 2, ( result, CONTINUE1left, SEMICOLON1right), rest671
)
end
|  ( 8, ( ( _, ( _, INT1left, INT1right)) :: rest671)) => let val  
result = MlyValue.Type (fn _ => (A.INT))
 in ( LrTable.NT 8, ( result, INT1left, INT1right), rest671)
end
|  ( 9, ( ( _, ( _, STRING1left, STRING1right)) :: rest671)) => let
 val  result = MlyValue.Type (fn _ => (A.STR))
 in ( LrTable.NT 8, ( result, STRING1left, STRING1right), rest671)
end
|  ( 10, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.Dec Dec1,
 _, _)) :: ( _, ( MlyValue.Type Type1, Type1left, _)) :: rest671)) =>
 let val  result = MlyValue.Decs (fn _ => let val  Type1 = Type1 ()
 val  (Dec as Dec1) = Dec1 ()
 in ( A.Dec (A.INT, Dec ) )
end)
 in ( LrTable.NT 9, ( result, Type1left, SEMICOLON1right), rest671)

end
|  ( 11, ( ( _, ( MlyValue.ID ID1, ID1left, ID1right)) :: rest671)) =>
 let val  result = MlyValue.Dec (fn _ => let val  (ID as ID1) = ID1 ()
 in ([A.Var(ID)])
end)
 in ( LrTable.NT 10, ( result, ID1left, ID1right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.Decr Decr1, _, Decr1right)) :: _ :: ( _, ( 
MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = 
MlyValue.Dec (fn _ => let val  (ID as ID1) = ID1 ()
 val  (Decr as Decr1) = Decr1 ()
 in (A.Var(ID)::Decr)
end)
 in ( LrTable.NT 10, ( result, ID1left, Decr1right), rest671)
end
|  ( 13, ( ( _, ( MlyValue.ID ID1, ID1left, ID1right)) :: rest671)) =>
 let val  result = MlyValue.Decr (fn _ => let val  (ID as ID1) = ID1
 ()
 in ([A.Var(ID)])
end)
 in ( LrTable.NT 11, ( result, ID1left, ID1right), rest671)
end
|  ( 14, ( ( _, ( MlyValue.Dec Dec1, _, Dec1right)) :: _ :: ( _, ( 
MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = 
MlyValue.Decr (fn _ => let val  (ID as ID1) = ID1 ()
 val  (Dec as Dec1) = Dec1 ()
 in (A.Var(ID)::Dec)
end)
 in ( LrTable.NT 11, ( result, ID1left, Dec1right), rest671)
end
|  ( 15, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.MathExp 
MathExp1, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, ID1left, _)) :: 
rest671)) => let val  result = MlyValue.Assignment (fn _ => let val  (
ID as ID1) = ID1 ()
 val  (MathExp as MathExp1) = MathExp1 ()
 in ( A.Assign ( A.Var (ID), MathExp )  )
end)
 in ( LrTable.NT 3, ( result, ID1left, SEMICOLON1right), rest671)
end
|  ( 16, ( ( _, ( MlyValue.IntConst IntConst1, IntConst1left, 
IntConst1right)) :: rest671)) => let val  result = MlyValue.MathExp
 (fn _ => let val  (IntConst as IntConst1) = IntConst1 ()
 in ( A.IntExp(IntConst))
end)
 in ( LrTable.NT 6, ( result, IntConst1left, IntConst1right), rest671)

end
|  ( 17, ( ( _, ( MlyValue.ID ID1, ID1left, ID1right)) :: rest671)) =>
 let val  result = MlyValue.MathExp (fn _ => let val  (ID as ID1) = 
ID1 ()
 in ( A.VarExp( A.Var(ID) ) )
end)
 in ( LrTable.NT 6, ( result, ID1left, ID1right), rest671)
end
|  ( 18, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.MathExp 
MathExp1, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let
 val  result = MlyValue.MathExp (fn _ => let val  (MathExp as MathExp1
) = MathExp1 ()
 in (MathExp)
end)
 in ( LrTable.NT 6, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 19, ( ( _, ( MlyValue.MathExp MathExp2, _, MathExp2right)) :: _
 :: ( _, ( MlyValue.MathExp MathExp1, MathExp1left, _)) :: rest671))
 => let val  result = MlyValue.MathExp (fn _ => let val  MathExp1 = 
MathExp1 ()
 val  MathExp2 = MathExp2 ()
 in ( A.BinE  ( MathExp1, A.Plus, MathExp2 ) )
end)
 in ( LrTable.NT 6, ( result, MathExp1left, MathExp2right), rest671)

end
|  ( 20, ( ( _, ( MlyValue.MathExp MathExp2, _, MathExp2right)) :: _
 :: ( _, ( MlyValue.MathExp MathExp1, MathExp1left, _)) :: rest671))
 => let val  result = MlyValue.MathExp (fn _ => let val  MathExp1 = 
MathExp1 ()
 val  MathExp2 = MathExp2 ()
 in ( A.BinE  ( MathExp1, A.Minus, MathExp2 ) )
end)
 in ( LrTable.NT 6, ( result, MathExp1left, MathExp2right), rest671)

end
|  ( 21, ( ( _, ( MlyValue.MathExp MathExp2, _, MathExp2right)) :: _
 :: ( _, ( MlyValue.MathExp MathExp1, MathExp1left, _)) :: rest671))
 => let val  result = MlyValue.MathExp (fn _ => let val  MathExp1 = 
MathExp1 ()
 val  MathExp2 = MathExp2 ()
 in ( A.BinE   ( MathExp1, A.Mul, MathExp2 ) )
end)
 in ( LrTable.NT 6, ( result, MathExp1left, MathExp2right), rest671)

end
|  ( 22, ( ( _, ( MlyValue.MathExp MathExp2, _, MathExp2right)) :: _
 :: ( _, ( MlyValue.MathExp MathExp1, MathExp1left, _)) :: rest671))
 => let val  result = MlyValue.MathExp (fn _ => let val  MathExp1 = 
MathExp1 ()
 val  MathExp2 = MathExp2 ()
 in ( A.BinE   ( MathExp1, A.Div, MathExp2 ) )
end)
 in ( LrTable.NT 6, ( result, MathExp1left, MathExp2right), rest671)

end
|  ( 23, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( 
MlyValue.Statements Statements1, _, _)) :: _ :: _ :: ( _, ( 
MlyValue.Condition Condition1, _, _)) :: _ :: ( _, ( _, IF1left, _))
 :: rest671)) => let val  result = MlyValue.Control (fn _ => let val 
 (Condition as Condition1) = Condition1 ()
 val  (Statements as Statements1) = Statements1 ()
 in ( A.If (Condition,Statements) )
end)
 in ( LrTable.NT 5, ( result, IF1left, SEMICOLON1right), rest671)
end
|  ( 24, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( 
MlyValue.Statements Statements2, _, _)) :: _ :: ( _, ( 
MlyValue.Statements Statements1, _, _)) :: _ :: _ :: ( _, ( 
MlyValue.Condition Condition1, _, _)) :: _ :: ( _, ( _, IF1left, _))
 :: rest671)) => let val  result = MlyValue.Control (fn _ => let val 
 (Condition as Condition1) = Condition1 ()
 val  (Statements as Statements1) = Statements1 ()
 val  Statements2 = Statements2 ()
 in (A.IfElse (Condition,Statements,Statements))
end)
 in ( LrTable.NT 5, ( result, IF1left, SEMICOLON1right), rest671)
end
|  ( 25, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( 
MlyValue.Statements Statements1, _, _)) :: _ :: _ :: ( _, ( 
MlyValue.Condition Condition1, _, _)) :: _ :: ( _, ( _, WHILE1left, _)
) :: rest671)) => let val  result = MlyValue.Control (fn _ => let val 
 (Condition as Condition1) = Condition1 ()
 val  (Statements as Statements1) = Statements1 ()
 in ( A.While (Condition,Statements) )
end)
 in ( LrTable.NT 5, ( result, WHILE1left, SEMICOLON1right), rest671)

end
|  ( 26, ( ( _, ( _, EQ1left, EQ1right)) :: rest671)) => let val  
result = MlyValue.RelOp (fn _ => (A.EqOp))
 in ( LrTable.NT 7, ( result, EQ1left, EQ1right), rest671)
end
|  ( 27, ( ( _, ( _, NEQ1left, NEQ1right)) :: rest671)) => let val  
result = MlyValue.RelOp (fn _ => (A.NeqOp))
 in ( LrTable.NT 7, ( result, NEQ1left, NEQ1right), rest671)
end
|  ( 28, ( ( _, ( _, LT1left, LT1right)) :: rest671)) => let val  
result = MlyValue.RelOp (fn _ => (A.LtOp))
 in ( LrTable.NT 7, ( result, LT1left, LT1right), rest671)
end
|  ( 29, ( ( _, ( _, LE1left, LE1right)) :: rest671)) => let val  
result = MlyValue.RelOp (fn _ => (A.LteOp))
 in ( LrTable.NT 7, ( result, LE1left, LE1right), rest671)
end
|  ( 30, ( ( _, ( _, GT1left, GT1right)) :: rest671)) => let val  
result = MlyValue.RelOp (fn _ => (A.GtOp))
 in ( LrTable.NT 7, ( result, GT1left, GT1right), rest671)
end
|  ( 31, ( ( _, ( _, GE1left, GE1right)) :: rest671)) => let val  
result = MlyValue.RelOp (fn _ => (A.GteOp))
 in ( LrTable.NT 7, ( result, GE1left, GE1right), rest671)
end
|  ( 32, ( ( _, ( _, AND1left, AND1right)) :: rest671)) => let val  
result = MlyValue.LoOp (fn _ => (A.AndOp))
 in ( LrTable.NT 12, ( result, AND1left, AND1right), rest671)
end
|  ( 33, ( ( _, ( _, OR1left, OR1right)) :: rest671)) => let val  
result = MlyValue.LoOp (fn _ => (A.OrOp))
 in ( LrTable.NT 12, ( result, OR1left, OR1right), rest671)
end
|  ( 34, ( ( _, ( MlyValue.MathExp MathExp2, _, MathExp2right)) :: ( _
, ( MlyValue.RelOp RelOp1, _, _)) :: ( _, ( MlyValue.MathExp MathExp1,
 MathExp1left, _)) :: rest671)) => let val  result = 
MlyValue.Condition (fn _ => let val  MathExp1 = MathExp1 ()
 val  (RelOp as RelOp1) = RelOp1 ()
 val  MathExp2 = MathExp2 ()
 in (A.RelC(MathExp1,RelOp,MathExp2))
end)
 in ( LrTable.NT 4, ( result, MathExp1left, MathExp2right), rest671)

end
|  ( 35, ( ( _, ( MlyValue.Condition Condition2, _, Condition2right))
 :: ( _, ( MlyValue.LoOp LoOp1, _, _)) :: ( _, ( MlyValue.Condition 
Condition1, Condition1left, _)) :: rest671)) => let val  result = 
MlyValue.Condition (fn _ => let val  Condition1 = Condition1 ()
 val  (LoOp as LoOp1) = LoOp1 ()
 val  Condition2 = Condition2 ()
 in (A.LoC(Condition1,LoOp,Condition2))
end)
 in ( LrTable.NT 4, ( result, Condition1left, Condition2right), 
rest671)
end
|  ( 36, ( ( _, ( MlyValue.MathExp MathExp1, MathExp1left, 
MathExp1right)) :: rest671)) => let val  result = MlyValue.Condition
 (fn _ => let val  (MathExp as MathExp1) = MathExp1 ()
 in (A.ExpC(MathExp))
end)
 in ( LrTable.NT 4, ( result, MathExp1left, MathExp1right), rest671)

end
|  ( 37, ( ( _, ( MlyValue.Condition Condition1, _, Condition1right))
 :: ( _, ( _, NOT1left, _)) :: rest671)) => let val  result = 
MlyValue.Condition (fn _ => let val  (Condition as Condition1) = 
Condition1 ()
 in (A.NotC(Condition))
end)
 in ( LrTable.NT 4, ( result, NOT1left, Condition1right), rest671)
end
|  ( 38, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.Condition 
Condition1, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let
 val  result = MlyValue.Condition (fn _ => let val  (Condition as 
Condition1) = Condition1 ()
 in (Condition)
end)
 in ( LrTable.NT 4, ( result, LPAREN1left, RPAREN1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.Program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : CL_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.ID (fn () => i),p1,p2))
fun INT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun STRING (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun IntConst (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.IntConst (fn () => i),p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMICOLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun MUL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun DIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun NEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun LE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun GE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun AND (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun OR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun NOT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun ASSIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VOID,p1,p2))
fun ENDIF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun DO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun BREAK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun CONTINUE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun ENDWHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
end
end
