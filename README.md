# Source code #


driver.sml		-	takes every token from the code which has to be compiled until EOF is found.

errormsg.sml		-	any error while lexing is found by errormsg.sml	and gives a message regarding the error.

token1.sig		-	signature of the structure is written in this file.
	
tokens1.sml		-	it contains the functions to take action when any key word has appeared in the given code.

mylex.lex(lexer)	-	the lex file is mylex.lex, takes action when we find any key word or identifier.

astn.sml		-	file to create abstract syntax tree i.e. printing the respective action taken when it finds any operator.

mylex.lex(parser)	-	it matches with the regular expression specified in the lex file and take appropriate action.

exprn.grm		-	yacc file for parsing.

parser.sml		-	joins the lexer and parser and calls the structure from grm to parse.


# Implementation #

Make sure that the test file is in both lexer and parser directory

# How to run lexer #

Go to lexer directory and give these commands in sml shell

CM.make "sources.cm"	

ParseLex.parse "filename"


# How to run parser #

Go to parser directory and give these commands in sml shell

CM.make "sources.cm"

Parser.parse "filename"










